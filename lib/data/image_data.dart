import 'package:instagram/models/post_model.dart';

class ImageData extends PostData {
  const ImageData({
    required super.id,
    required super.name,
    required super.avatar,
    required super.location,
    required super.messageImage,
    required super.listLikes,
    required super.messageText,
    required super.comments,
    required super.reel,
  });
}

const imageList = [
  ImageData(
    id: 'id-101',
    messageImage:
        'https://playua.net/wp-content/uploads/2015/12/5670217c34329d4ad4b8a6e1f93cd964.jpg',
    name: 'keverdeen',
    avatar:
        'https://i.pinimg.com/564x/7c/9d/7a/7c9d7aca2bae72ff52247c7f29ae9251.jpg',
    listLikes: {'ghawthorne', 'ahaymitch'},
    messageText: 'Sometimes I fight the Capitol',
    comments: 110,
    location: '',
    reel: false,
  ),
  ImageData(
    id: 'id-102',
    messageImage: 'https://kor.ill.in.ua/m/610x385/2313649.jpg',
    name: 'keverdeen',
    avatar:
        'https://i.pinimg.com/564x/7c/9d/7a/7c9d7aca2bae72ff52247c7f29ae9251.jpg',
    listLikes: {'ghawthorne', 'ahaymitch'},
    messageText: 'We\'re at The Hunger Games',
    comments: 110,
    reel: false,
    location: '',
  ),
  ImageData(
    id: 'id-103',
    messageImage: 'https://www.kinofilms.ua/images/media/2020/4/23/1.jpg',
    name: 'etrinket',
    avatar:
        'https://i.pinimg.com/564x/f8/df/99/f8df995365b232c8f3085f63ee7af36f.jpg',
    listLikes: {'ghawthorne', 'ahaymitch'},
    messageText: 'Let\'s pick the Tributes for the next Hunger Games!',
    comments: 110,
    location: '',
    reel: false,
  ),
  ImageData(
    id: 'id-104',
    messageImage: 'https://kor.ill.in.ua/m/1200x0/1636800.jpg',
    name: 'keverdeen',
    avatar:
        'https://i.pinimg.com/564x/7c/9d/7a/7c9d7aca2bae72ff52247c7f29ae9251.jpg',
    listLikes: {'ghawthorne', 'ahaymitch'},
    messageText: 'Chill out)))',
    comments: 110,
    location: '',
    reel: false,
  ),
  ImageData(
    id: 'id-105',
    messageImage:
        'https://lenta.ua/storage/03/24/09/01/890_450_5eda7bf656566.jpg',
    name: 'snow',
    avatar:
        'https://i.pinimg.com/originals/1e/bc/56/1ebc56ee7c2a49cdc119e31f329d4668.jpg',
    listLikes: {'ghawthorne', 'ahaymitch'},
    messageText: 'Welcoming the Tributes to the Capitol!',
    comments: 110,
    location: '',
    reel: false,
  ),
  ImageData(
    id: 'id-106',
    messageImage: 'https://wallpapers-fenix.eu/full/180814/222234117.jpg',
    name: 'keverdeen',
    avatar:
        'https://i.pinimg.com/564x/7c/9d/7a/7c9d7aca2bae72ff52247c7f29ae9251.jpg',
    listLikes: {'ghawthorne', 'ahaymitch'},
    messageText: 'My lovely team <3',
    comments: 110,
    location: "",
    reel: false,
  ),
  ImageData(
    id: 'id-107',
    messageImage: 'https://vorobus.com/wp-content/uploads/2013/11/u-vogny.jpg',
    name: 'keverdeen',
    avatar:
        'https://i.pinimg.com/564x/7c/9d/7a/7c9d7aca2bae72ff52247c7f29ae9251.jpg',
    listLikes: {'ghawthorne', 'ahaymitch'},
    messageText: 'The WOW effect',
    comments: 110,
    location: "",
    reel: false,
  ),
  ImageData(
    id: 'id-108',
    messageImage:
        'https://itc.ua/wp-content/uploads/2024/01/The-Hunger-Games.jpg',
    name: 'keverdeen',
    avatar:
        'https://i.pinimg.com/564x/7c/9d/7a/7c9d7aca2bae72ff52247c7f29ae9251.jpg',
    listLikes: {'ghawthorne', 'ahaymitch'},
    messageText: 'Sometimes I fight the Capitol',
    comments: 110,
    location: '',
    reel: false,
  ),
  ImageData(
    id: 'id-109',
    messageImage:
        'https://m.kinoafisha.ua/upload/2014/11/films/6028/fotos/1415112877golodne-igr-soika-peresmeshnica-cast-i.jpg',
    name: 'keverdeen',
    avatar:
        'https://i.pinimg.com/564x/7c/9d/7a/7c9d7aca2bae72ff52247c7f29ae9251.jpg',
    listLikes: {'ghawthorne', 'ahaymitch'},
    messageText: 'We\'re discussing plans',
    comments: 110,
    location: '',
    reel: false,
  ),
  ImageData(
    id: 'id-110',
    messageImage:
        'https://novy.tv/wp-content/uploads/sites/96/2016/11/game-2.jpg',
    name: 'keverdeen',
    avatar:
        'https://i.pinimg.com/564x/7c/9d/7a/7c9d7aca2bae72ff52247c7f29ae9251.jpg',
    listLikes: {'ghawthorne', 'ahaymitch'},
    messageText: 'Riding the winner\'s train',
    comments: 110,
    location: '',
    reel: false,
  ),
  ImageData(
    id: 'id-111',
    messageImage:
        'https://karavan.ua/wp-content/uploads/2023/04/The-Hunger-Games-The-Ballad-Of-Songbirds-and-Snakes-6.jpg',
    name: 'snow',
    avatar:
        'https://i.pinimg.com/originals/1e/bc/56/1ebc56ee7c2a49cdc119e31f329d4668.jpg',
    listLikes: {'ghawthorne', 'ahaymitch'},
    messageText: 'I was a top student at Capitol Academy.',
    comments: 110,
    location: '',
    reel: false,
  ),
  ImageData(
    id: 'id-112',
    messageImage:
        'https://img.otzyvua.net/2023/11/16/film-golodnye-igry-ballada-o-zmeyah_6555c00ae64d7.jpg',
    name: 'etrinket',
    avatar:
        'https://i.pinimg.com/564x/f8/df/99/f8df995365b232c8f3085f63ee7af36f.jpg',
    listLikes: {'ghawthorne', 'ahaymitch'},
    messageText: 'Met a friend. Talked about outfits.)',
    comments: 110,
    location: '',
    reel: false,
  ),
  ImageData(
    id: 'id-113',
    messageImage: 'https://kor.ill.in.ua/m/610x385/2313649.jpg',
    name: 'keverdeen',
    avatar:
        'https://i.pinimg.com/564x/7c/9d/7a/7c9d7aca2bae72ff52247c7f29ae9251.jpg',
    listLikes: {'ghawthorne', 'ahaymitch'},
    messageText: 'We\'re at The Hunger Games',
    comments: 110,
    location: "",
    reel: false,
  ),
  ImageData(
    id: 'id-114',
    messageImage: 'https://www.kinofilms.ua/images/media/2020/4/23/1.jpg',
    name: 'etrinket',
    avatar:
        'https://i.pinimg.com/564x/f8/df/99/f8df995365b232c8f3085f63ee7af36f.jpg',
    listLikes: {'ghawthorne', 'ahaymitch'},
    messageText: 'Let\'s pick the Tributes for the next Hunger Games!',
    comments: 110,
    location: '',
    reel: false,
  ),
  ImageData(
    id: 'id-115',
    messageImage: 'https://kor.ill.in.ua/m/1200x0/1636800.jpg',
    name: 'keverdeen',
    avatar:
        'https://i.pinimg.com/564x/7c/9d/7a/7c9d7aca2bae72ff52247c7f29ae9251.jpg',
    listLikes: {'ghawthorne', 'ahaymitch'},
    messageText: 'Chill out)))',
    comments: 110,
    location: '',
    reel: false,
  ),
];
