class MyProfileData {
  final String id;
  final String nickname;
  final String name;
  final String avatar;
  final int followers;
  final int following;

  const MyProfileData({
    required this.id,
    required this.nickname,
    required this.name,
    required this.avatar,
    required this.followers,
    required this.following,
  });
}

const myProfileData = MyProfileData(
  id: 'id-001',
  nickname: 'pita_m',
  name: 'Pita Mellark',
  avatar: 'https://2img.net/u/3412/19/22/63/avatars/110530-94.png',
  followers: 0,
  following: 5,
);
