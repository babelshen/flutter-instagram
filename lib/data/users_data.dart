import 'package:instagram/models/post_model.dart';

class UsersData extends PostData {
  const UsersData({
    required super.id,
    required super.name,
    required super.avatar,
    required super.location,
    required super.messageImage,
    required super.listLikes,
    required super.messageText,
    required super.comments,
    required super.reel,
  });
}

var usersList = [
  const UsersData(
    id: 'id-001',
    name: 'keverdeen',
    avatar:
        'https://i.pinimg.com/564x/7c/9d/7a/7c9d7aca2bae72ff52247c7f29ae9251.jpg',
    location: 'Forest | District 12',
    messageImage:
        'https://kinokong.day/uploads/posts/2023-06/1686344792_1jpg.png',
    messageText:
        'Love hunting! Today I got two ducks and one moose. I barely made it home)))',
    comments: 110,
    listLikes: {'ghawthorne', 'ahaymitch'},
    reel: true,
  ),
  const UsersData(
    id: 'id-002',
    name: 'ghawthorne',
    avatar:
        'https://a1cf74336522e87f135f-2f21ace9a6cf0052456644b80fa06d4f.ssl.cf2.rackcdn.com/images/characters/large/800/Gale-Hawthorne.The-Hunger-Games.webp',
    location: 'Mine | District 12',
    messageImage:
        'https://bookstacked.com/wp-content/uploads/2015/11/catching-fire-image08.jpg',
    messageText:
        'Tough day at the mine, but we did it! Thank you, friends, for your help! Today we fulfilled our coal mining norm!',
    comments: 25,
    listLikes: {'keverdeen', 'ahaymitch'},
    reel: false,
  ),
  const UsersData(
    id: 'id-003',
    name: 'ahaymitch',
    avatar:
        'https://i.pinimg.com/200x150/cb/b6/1e/cbb61e1edf24d6b12ecc1d89bd591809.jpg',
    location: "Winners' Village",
    messageImage:
        'https://i.pinimg.com/originals/fa/23/89/fa23897b1e3b2e28694da519748ed057.jpg',
    messageText: 'Nice party!',
    comments: 250,
    listLikes: {'keverdeen', 'ghawthorne'},
    reel: true,
  ),
  const UsersData(
    id: 'id-004',
    name: 'snow',
    avatar:
        'https://i.pinimg.com/originals/1e/bc/56/1ebc56ee7c2a49cdc119e31f329d4668.jpg',
    location: 'Presidential Palace | Capitol',
    messageImage:
        'https://cdn.mos.cms.futurecdn.net/E7pzw28HzV3KubBqjgDz9D-1200-80.jpg',
    messageText:
        'Solving capitol issues. Tomorrow we will be taking up a number of issues in the Senate to ensure order in the districts.',
    comments: 120,
    listLikes: {'snow', 'ahaymitch'},
    reel: false,
  ),
  const UsersData(
    id: 'id-005',
    name: 'etrinket',
    avatar:
        'https://i.pinimg.com/564x/f8/df/99/f8df995365b232c8f3085f63ee7af36f.jpg',
    location: 'Rose Garden | Capitol',
    messageImage:
        'https://i.pinimg.com/564x/ec/76/ac/ec76ac394393a7f25876b6009293d5e4.jpg',
    messageText: 'Bought a new dress ^_^',
    comments: 180,
    listLikes: {
      'keverdeen',
      'ghawthorne',
    },
    reel: true,
  ),
];
