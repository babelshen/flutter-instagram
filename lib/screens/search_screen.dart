import 'package:flutter/material.dart';
import 'package:instagram/data/image_data.dart';
import 'package:instagram/widgets/ui/image_detail.dart';
import 'package:instagram/widgets/search/search_input.dart';
import 'package:instagram/widgets/ui/image_cart.dart';

class SearchScreen extends StatelessWidget {
  const SearchScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: SearchInput(),
      ),
      body: GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          mainAxisSpacing: 1,
          crossAxisSpacing: 1,
        ),
        itemCount: imageList.length,
        itemBuilder: (context, index) {
          final imageData = imageList[index];
          return InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ImageDetailScreen(imageData: imageData),
                ),
              );
            },
            child: Hero(
              tag: imageData.id,
              child: ImageCart(imageData: imageData),
            ),
          );
        },
      ),
    );
  }
}
