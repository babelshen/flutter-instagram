import 'package:flutter/material.dart';
import 'package:instagram/data/myprofile_data.dart';
import 'package:instagram/models/load_image_modal.dart';
import 'package:instagram/widgets/ui/edit_comment_field.dart';
import 'package:instagram/widgets/profile/profile_app_bar.dart';
import 'package:instagram/widgets/profile/profile_body.dart';

class ProfileScreen extends StatefulWidget {
  final List<ImageModel> uploadedImages;
  final void Function(int) onDeletePost;

  const ProfileScreen(
      {super.key, required this.uploadedImages, required this.onDeletePost});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  String myComment = '';

  void _editProfile() async {
    final String? comment = await showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return const EditCommentField();
      },
    );

    if (comment != null) {
      setState(() {
        myComment = comment;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 3,
      child: Scaffold(
        appBar: ProfileAppBar(nickname: myProfileData.nickname),
        body: ProfileBody(
          uploadedImages: widget.uploadedImages,
          myComment: myComment,
          editProfileCallback: _editProfile,
          avatar: myProfileData.avatar,
          following: myProfileData.following,
          followers: myProfileData.followers,
          name: myProfileData.name,
          onDeletePost: widget.onDeletePost,
        ),
      ),
    );
  }
}
