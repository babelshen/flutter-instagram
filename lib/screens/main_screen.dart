import 'package:flutter/material.dart';
import 'package:instagram/models/load_image_modal.dart';
import 'package:instagram/screens/home_screen.dart';
import 'package:instagram/screens/profile_screen.dart';
import 'package:instagram/screens/saved_screen.dart';
import 'package:instagram/screens/search_screen.dart';
import 'package:instagram/screens/shop_screen.dart';
import 'package:instagram/widgets/ui/navigation.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final int _currentIndex = 0;
  final List<ImageModel> _uploadedImages = [];

  late final List<Widget> _listOptions;

  @override
  void initState() {
    super.initState();
    _listOptions = [
      HomeScreen(
        onSavePost: (imageUrl, location, comment) {
          setState(() {
            _uploadedImages.add(ImageModel(
              id: DateTime.now().millisecondsSinceEpoch,
              imageUrl: imageUrl,
              location: location,
              comment: comment,
            ));
          });
        },
      ),
      const SearchScreen(),
      const Shopscreen(),
      const SavedScreen(),
      ProfileScreen(
          uploadedImages: _uploadedImages,
          onDeletePost: (id) {
            setState(() {
              _uploadedImages.removeWhere((item) => item.id == id);
            });
          }),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Scaffold(
          body: Center(child: _listOptions.elementAt(_currentIndex)),
          bottomNavigationBar: NavigationWidget(
            onSavePost: (imageUrl, location, comment) {
              setState(() {
                _uploadedImages.add(ImageModel(
                  id: DateTime.now().microsecondsSinceEpoch,
                  imageUrl: imageUrl,
                  location: location,
                  comment: comment,
                ));
              });
            },
            onDeletePost: (int id) {
              setState(() {
                _uploadedImages.removeWhere((item) => item.id == id);
              });
            },
            uploadedImages: _uploadedImages,
          ),
        ),
      ),
    );
  }
}
