import 'package:flutter/material.dart';
import 'package:instagram/state/products_state.dart';
import 'package:instagram/widgets/shop/product_card.dart';
import 'package:provider/provider.dart';

class Shopscreen extends StatelessWidget {
  const Shopscreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Product List'),
      ),
      body: Center(
        child: Consumer<ProductProvider>(
          builder: (context, productList, _) {
            if (productList.products.isNotEmpty) {
              return ListView.builder(
                itemCount: productList.products.length,
                itemBuilder: (context, index) {
                  return ProductCard(product: productList.products[index]);
                },
              );
            } else {
              return const CircularProgressIndicator();
            }
          },
        ),
      ),
    );
  }
}
