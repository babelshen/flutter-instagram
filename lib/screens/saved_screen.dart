import 'package:flutter/material.dart';
import 'package:instagram/state/save_posts_state.dart';
import 'package:instagram/widgets/ui/image_detail.dart';
import 'package:instagram/widgets/ui/image_cart.dart';
import 'package:provider/provider.dart';

class SavedScreen extends StatelessWidget {
  const SavedScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<SavePostManage>(builder: (context, manage, child) {
      return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: const Text('Saved posts'),
        ),
        body: GridView.builder(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            mainAxisSpacing: 1,
            crossAxisSpacing: 1,
          ),
          itemCount: manage.amountSavedPosts,
          itemBuilder: (context, index) {
            final postData = manage.saveListPostsItems[index];
            return InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        ImageDetailScreen(imageData: postData),
                  ),
                );
              },
              child: Hero(
                tag: postData.id,
                child: ImageCart(imageData: postData),
              ),
            );
          },
        ),
      );
    });
  }
}
