import 'package:flutter/material.dart';
import 'package:instagram/data/users_data.dart';
import 'package:instagram/widgets/ui/custom_button_add_post_url.dart';
import 'package:instagram/widgets/ui/custom_drawer.dart';
import 'package:instagram/widgets/home/custom_app_bar.dart';
import 'package:instagram/widgets/home/posts_list_widget.dart';
import 'package:instagram/widgets/home/story_list_widget.dart';

class HomeScreen extends StatefulWidget {
  final void Function(String, String, String) onSavePost;
  const HomeScreen({super.key, required this.onSavePost});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool isFloatingActionButtonEnabled = true;
  FloatingActionButtonLocation? fabLocation;
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();

  void _addPost(UsersData newPost) {
    setState(() {
      usersList.insert(0, newPost);
      _listKey.currentState?.insertItem(0);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: CustomAppBar(),
      ),
      drawer: CustomDrawer(
        isFloatingActionButtonEnabled: isFloatingActionButtonEnabled,
        fabLocation: fabLocation,
        onFloatingActionButtonEnabledChanged: (value) {
          setState(() {
            isFloatingActionButtonEnabled = value;
          });
        },
        onFabLocationChanged: (newValue) {
          setState(() {
            fabLocation = newValue;
          });
        },
      ),
      body: Column(
        children: [
          const StoryListWidget(),
          PostListWidget(listKey: _listKey),
        ],
      ),
      floatingActionButtonLocation:
          fabLocation ?? FloatingActionButtonLocation.centerFloat,
      floatingActionButton: isFloatingActionButtonEnabled
          ? CustomFloatingActionButton(
              onAddPost: (newPost) {
                _addPost(newPost);
              },
              onSavePost: widget.onSavePost,
            )
          : null,
    );
  }
}
