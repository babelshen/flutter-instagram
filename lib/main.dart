import 'package:flutter/material.dart';
import 'package:instagram/screens/main_screen.dart';
import 'package:instagram/state/order_model.dart';
import 'package:instagram/state/character_state.dart';
import 'package:instagram/state/posts_counter_state.dart';
import 'package:instagram/state/products_state.dart';
import 'package:instagram/state/save_posts_state.dart';
import 'package:instagram/state/theme.model.dart';
import 'package:instagram/theme/dark_theme.dart';
import 'package:instagram/theme/light_theme.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  runApp(
    ChangeNotifierProvider<ChangeTheme>(
      create: (_) => ChangeTheme(preferences: prefs),
      child: MyApp(preferences: prefs),
    ),
  );
}

class MyApp extends StatelessWidget {
  final SharedPreferences preferences;
  const MyApp({super.key, required this.preferences});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: PostsCountManage()),
        ChangeNotifierProvider.value(value: SavePostManage()),
        ChangeNotifierProvider<ProductProvider>(
          create: (_) => ProductProvider()..fetchProducts(),
        ),
        ChangeNotifierProvider<CharacterProvider>(
          create: (_) => CharacterProvider()..fetchRandomCharacter(),
        ),
        ChangeNotifierProvider.value(value: OrdersState()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Instagram',
        theme: Provider.of<ChangeTheme>(context).isDarkThemeSelected() ? darkTheme : lightTheme,
        home: const MainScreen(),
      ),
    );
  }
}
