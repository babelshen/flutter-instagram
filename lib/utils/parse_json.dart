import 'dart:convert';

dynamic parseJson(String jsonString) {
  try {
    return jsonDecode(jsonString);
  } catch (e) {
    print('Error parsing JSON: $e');
    return null;
  }
}