import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:instagram/models/product_model.dart';
import 'package:instagram/utils/parse_json.dart';

class ProductProvider extends ChangeNotifier {
  List<Product> _products = [];

  List<Product> get products => _products;

  Future<void> fetchProducts() async {
    final response =
        await http.get(Uri.parse('https://fakestoreapi.com/products'));

    if (response.statusCode == 200) {
      Iterable productsJson = parseJson(response.body);
      _products = productsJson.map((model) => Product.fromJson(model)).toList();
      notifyListeners();
    } else {
      throw Exception('Failed to load products');
    }
  }
}
