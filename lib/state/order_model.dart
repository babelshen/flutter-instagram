import 'package:flutter/material.dart';
import 'package:instagram/models/product_model.dart';

class OrdersState extends ChangeNotifier {
  final List<Product> _orderedProducts = [];

  List<Product> get orderedProducts => _orderedProducts;

  void addOrder(Product product) {
    _orderedProducts.add(product);
    notifyListeners();
  }

  void removeOrderById(int productId) {
    _orderedProducts.removeWhere((product) => product.id == productId);
    notifyListeners();
  }
}
