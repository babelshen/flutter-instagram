import 'dart:math';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:instagram/models/character_model.dart';
import 'package:instagram/utils/parse_json.dart';

class CharacterProvider extends ChangeNotifier {
  late Character _character;

  Character get randomCharacter => _character;

  Future<void> fetchRandomCharacter() {
    final randomId = 1 + Random().nextInt(100);

    return http
        .get(Uri.parse('https://rickandmortyapi.com/api/character/$randomId/'))
        .then((response) {
      final characterJson = parseJson(response.body);
      _character = Character.fromJson(characterJson);
      notifyListeners();
    }).catchError((error) {
      throw Exception('Failed to load character: $error');
    });
  }
}
