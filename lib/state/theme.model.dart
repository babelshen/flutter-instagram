import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChangeTheme extends ChangeNotifier {
  late SharedPreferences _preferences;

  static const String _isDarkThemeSelectedKey = 'dark_theme_selected';

  ChangeTheme({required SharedPreferences preferences}) {
    _preferences = preferences;
  }

  bool isDarkThemeSelected() {
    final selected = _preferences.getBool(_isDarkThemeSelectedKey);
    return selected ?? false;
  }

  Future<void> setDarkThemeSelected(bool selected) async {
    await _preferences.setBool(_isDarkThemeSelectedKey, selected);
    notifyListeners();
  }
}
