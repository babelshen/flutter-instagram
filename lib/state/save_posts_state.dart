import 'package:flutter/material.dart';
import 'package:instagram/models/post_model.dart';

class SavePostManage extends ChangeNotifier {
  List<PostData> saveListPosts = [];

  List<PostData> get saveListPostsItems {
    return saveListPosts;
  }

  int get amountSavedPosts {
    return saveListPosts.length;
  }

  bool checkSavePost(String id) {
    return saveListPosts.any((post) => id == post.id);
  }

  void addPost(PostData post) {
    saveListPosts.add(post);
    notifyListeners();
  }

  void removePost(String id) {
    saveListPosts.removeWhere((post) => id == post.id);
    notifyListeners();
  }
}
