import 'package:flutter/material.dart';

class PostsCountManage extends ChangeNotifier {
  int _posts = 0;

  int get postsCount => _posts;

  void addPostsCounter() {
    _posts++;
    notifyListeners();
  }

  void removePostsCounter() {
    _posts--;
    notifyListeners();
  }
}
