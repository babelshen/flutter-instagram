import 'package:flutter/material.dart';

final darkTheme = ThemeData(
  primaryColor: Colors.white,
  primarySwatch: Colors.green, 
  colorScheme: ColorScheme.fromSeed(
    seedColor: Colors.black,
    brightness: Brightness.dark,
    surfaceTint: Colors.black,
    surface: Colors.black,
  ),
  useMaterial3: true,
  scaffoldBackgroundColor: Colors.black,
  drawerTheme: const DrawerThemeData(
    backgroundColor: Colors.black,
    scrimColor: Color.fromARGB(90, 0, 0, 0),
    shape: Border.fromBorderSide(
      BorderSide(
        color: Color.fromARGB(255, 37, 37, 37),
        width: 1,
      )
    ),
  ),
  floatingActionButtonTheme: const FloatingActionButtonThemeData(
    backgroundColor: Color.fromARGB(255, 27, 133, 220),
    focusColor: Color.fromARGB(255, 103, 186, 255),
    splashColor: Color.fromARGB(255, 103, 186, 255),
  ),
  bottomNavigationBarTheme: const BottomNavigationBarThemeData(
    backgroundColor: Colors.black,
    selectedItemColor: Colors.white,
    unselectedItemColor: Color.fromARGB(255, 96, 96, 96),
  ),
  tabBarTheme: const TabBarTheme(
    dividerColor: Colors.transparent,
    unselectedLabelColor: Color.fromARGB(255, 103, 103, 103),
    labelColor: Colors.white,
    indicator: BoxDecoration(
      border: Border(
        bottom: BorderSide(color: Colors.white, width: 1),
      ),
    ),
  ),
  iconTheme: const IconThemeData(
    color: Colors.white,
  ),
  textTheme: const TextTheme(
    titleLarge: TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.bold,
      fontSize: 22,
    ),
    titleMedium: TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.bold,
      fontSize: 14,
    ),
    titleSmall: TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.normal,
      fontSize: 14,
    ),
    labelLarge: TextStyle(
      color: Colors.white,
      fontSize: 16,
      fontWeight: FontWeight.bold,
    ),
    labelMedium: TextStyle(
      color: Colors.grey,
      fontWeight: FontWeight.normal,
      fontSize: 14,
    ),
    labelSmall: TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.normal,
      fontSize: 12,
    ),
  ),
);
