import 'package:flutter/material.dart';

final lightTheme = ThemeData(
  primaryColor: Colors.black,
  primarySwatch: Colors.green, 
  colorScheme: ColorScheme.fromSeed(
    seedColor: Colors.white,
    brightness: Brightness.light,
    surfaceTint: Colors.white,
    surface: Colors.white,
  ),
  useMaterial3: true,
  scaffoldBackgroundColor: Colors.white,
  drawerTheme: const DrawerThemeData(
    backgroundColor: Colors.white,
    scrimColor: Color.fromARGB(90, 0, 0, 0),
    shape: Border.fromBorderSide(
      BorderSide(
        color: Colors.grey,
        width: 1,
      )
    ),
  ),
  tabBarTheme: const TabBarTheme(
    dividerColor: Colors.transparent,
    unselectedLabelColor: Colors.grey,
    labelColor: Colors.black,
    indicator: BoxDecoration(
      border: Border(
        bottom: BorderSide(color: Colors.black, width: 1),
      ),
    ),
  ),
  iconTheme: const IconThemeData(
    color: Colors.black,
  ),
  bottomNavigationBarTheme: const BottomNavigationBarThemeData(
    backgroundColor: Colors.white,
    selectedItemColor: Colors.black,
    unselectedItemColor: Colors.grey,
  ),
  floatingActionButtonTheme: const FloatingActionButtonThemeData(
    backgroundColor: Color.fromARGB(255, 181, 222, 255),
    focusColor: Color.fromARGB(255, 103, 186, 255),
    splashColor: Color.fromARGB(255, 103, 186, 255),
  ),
  textTheme: const TextTheme(
    titleLarge: TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.bold,
      fontSize: 22,
    ),
    titleMedium: TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.bold,
      fontSize: 14,
    ),
    titleSmall: TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.normal,
      fontSize: 14,
    ),
    labelLarge: TextStyle(
      color: Colors.black,
      fontSize: 16,
      fontWeight: FontWeight.bold,
    ),
    labelMedium: TextStyle(
      color: Colors.grey,
      fontWeight: FontWeight.normal,
      fontSize: 14,
    ),
    labelSmall: TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.normal,
      fontSize: 12,
    ),
  ),
);
