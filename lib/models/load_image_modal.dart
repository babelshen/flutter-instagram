class ImageModel {
  final int id;
  final String imageUrl;
  final String location;
  final String comment;

  ImageModel({
    required this.id,
    required this.imageUrl,
    required this.location,
    required this.comment,
  });
}
