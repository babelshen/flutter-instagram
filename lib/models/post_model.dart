abstract class PostData {
  final String id;
  final String name;
  final String avatar;
  final Set<String> listLikes;
  final String messageText;
  final int comments;
  final bool reel;
  final String location;
  final String messageImage;

  const PostData({
    required this.id,
    required this.name,
    required this.avatar,
    required this.listLikes,
    required this.messageText,
    required this.comments,
    required this.reel,
    required this.location,
    required this.messageImage,
  });
}
