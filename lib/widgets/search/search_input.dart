import 'package:flutter/material.dart';

class SearchInput extends StatelessWidget {
  SearchInput({super.key});

  final TextEditingController _textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: 40,
      child: TextField(
        controller: _textController,
        decoration: InputDecoration(
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide.none),
          filled: true,
          prefixIcon: const Icon(Icons.search, color: Colors.grey),
          hintStyle: theme.textTheme.labelMedium,
          hintText: 'Search',
          contentPadding: const EdgeInsets.symmetric(vertical: 10),
        ),
      ),
    );
  }
}
