import 'package:flutter/material.dart';
import 'package:instagram/models/product_model.dart';
import 'package:instagram/state/order_model.dart';
import 'package:provider/provider.dart';

class ProductCard extends StatelessWidget {
  final Product product;

  const ProductCard({super.key, required this.product});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Image.network(
        product.image,
        width: 100,
        height: 100,
      ),
      title: Text(product.title),
      subtitle: Text(
        product.description.length <= 100
            ? product.description
            : '${product.description.substring(0, 100)}...',
      ),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text('\$${product.price}'),
          IconButton(
            onPressed: () {
              Provider.of<OrdersState>(context, listen: false)
                  .addOrder(product);
            },
            icon: const Icon(Icons.shopping_cart),
          ),
        ],
      ),
    );
  }
}
