import 'package:flutter/material.dart';
import 'package:instagram/data/users_data.dart';
import 'package:instagram/widgets/post/post_cart.dart';

class PostListWidget extends StatelessWidget {
  const PostListWidget({
    super.key,
    required this.listKey,
  });

  final GlobalKey<AnimatedListState> listKey;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: AnimatedList(
        key: listKey,
        initialItemCount: usersList.length,
        itemBuilder: (context, index, animation) {
          return SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(1, 0),
              end: Offset.zero,
            ).animate(animation),
            child: Column(
              children: [
                PostCart(index: index),
                const SizedBox(height: 10),
              ],
            ),
          );
        },
      ),
    );
  }
}
