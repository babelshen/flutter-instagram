import 'package:flutter/material.dart';
import 'package:instagram/data/users_data.dart';
import 'package:instagram/widgets/stories/story_widget.dart';

class StoryListWidget extends StatelessWidget {
  const StoryListWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 130,
      child: ListView.builder(
        itemCount: usersList.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          if (usersList[index].reel) {
            return StoryWidget(user: usersList[index]);
          } else {
            return const SizedBox();
          }
        },
      ),
    );
  }
}
