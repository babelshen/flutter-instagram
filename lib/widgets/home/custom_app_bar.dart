import 'package:flutter/material.dart';
import 'package:instagram/state/character_state.dart';
import 'package:instagram/state/theme.model.dart';
import 'package:provider/provider.dart';
import 'package:scratcher/scratcher.dart';

class CustomAppBar extends StatelessWidget {
  const CustomAppBar({super.key});

  Future<void> scratchCardDialog(BuildContext context) async {
  final theme = Theme.of(context);
  final characterProvider = Provider.of<CharacterProvider>(context, listen: false);

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return FutureBuilder(
        future: characterProvider.fetchRandomCharacter(),
        builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasError) {
            return AlertDialog(
              title: const Text('Error'),
              content: Text('Failed to load character: ${snapshot.error}'),
              actions: [
                TextButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: const Text('OK'),
                ),
              ],
            );
          } else {
            return AlertDialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
              title: Align(
                alignment: Alignment.center,
                child: Text(
                  'Which Rick and Morty character are you?',
                  style: theme.textTheme.labelLarge,
                ),
              ),
              content: Scratcher(
                accuracy: ScratchAccuracy.low,
                brushSize: 50,
                child: Container(
                  height: 300,
                  width: 300,
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.network(
                        characterProvider.randomCharacter.image,
                        width: 150,
                        height: 150,
                      ),
                      Text(
                        characterProvider.randomCharacter.name,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.blue,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }
        },
      );
    },
  );
}


  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      elevation: 0,
      leading: Builder(
        builder: (context) => IconButton(
          icon: const Icon(Icons.menu),
          onPressed: () {
            Scaffold.of(context).openDrawer();
          },
        ),
      ),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Image(
            image: const AssetImage('assets/instagram.png'),
            width: 120,
            color: Provider.of<ChangeTheme>(context).isDarkThemeSelected()
                ? Colors.white
                : Colors.black,
          ),
          Row(
            children: [
              const Icon(
                Icons.favorite_border_outlined,
              ),
              const SizedBox(width: 20),
              IconButton(
                icon: const Icon(Icons.catching_pokemon_rounded),
                onPressed: () => scratchCardDialog(context),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
