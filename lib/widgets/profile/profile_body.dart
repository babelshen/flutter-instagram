import 'package:flutter/material.dart';
import 'package:instagram/models/load_image_modal.dart';
import 'package:instagram/state/posts_counter_state.dart';
import 'package:instagram/widgets/modals/orders_modal_window.dart';
import 'package:instagram/widgets/ui/edit_profile_button.dart';
import 'package:instagram/widgets/home/story_list_widget.dart';
import 'package:instagram/widgets/profile/profile_grid.dart';
import 'package:instagram/widgets/profile/profile_header.dart';
import 'package:instagram/widgets/profile/profile_tab.dart';
import 'package:provider/provider.dart';

class ProfileBody extends StatelessWidget {
  final List<ImageModel> uploadedImages;
  final String myComment;
  final VoidCallback editProfileCallback;
  final String avatar;
  final int following;
  final int followers;
  final String name;
  final void Function(int) onDeletePost;

  const ProfileBody({
    super.key,
    required this.uploadedImages,
    required this.myComment,
    required this.editProfileCallback,
    required this.avatar,
    required this.following,
    required this.followers,
    required this.name,
    required this.onDeletePost,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    int postsCount = Provider.of<PostsCountManage>(context).postsCount;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ProfileHeaderWidget(
          avatar: avatar,
          following: following,
          followers: followers,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 10, top: 5),
          child: Text(
            name,
            style: theme.textTheme.labelLarge,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          child: Text(
            myComment,
            style: theme.textTheme.titleSmall,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              EditProfileButton(editProfileCallback: editProfileCallback),
              const SizedBox(width: 7),
              const OrdersModalWindow(),
            ],
          ),
        ),
        const StoryListWidget(),
        const TabBar(
          indicatorSize: TabBarIndicatorSize.tab,
          tabs: <Widget>[
            Tab(
              icon: Icon(Icons.grid_on_outlined),
            ),
            Tab(
              icon: Icon(Icons.smart_display_outlined),
            ),
            Tab(
              icon: Icon(Icons.person_pin_outlined),
            ),
          ],
        ),
        Expanded(
          child: TabBarView(
            children: <Widget>[
              Center(
                child: postsCount > 0
                    ? ProfileGrid(
                        uploadedImages: uploadedImages,
                        onDeletePost: onDeletePost)
                    : const CustomTabWidget(
                        tabText: 'No Posts Yet',
                        icon: Icons.camera_alt_outlined),
              ),
              const CustomTabWidget(
                  tabText: 'Add reels', icon: Icons.slow_motion_video_outlined),
              const CustomTabWidget(
                  tabText: 'Your photos and videos', icon: Icons.person_add),
            ],
          ),
        ),
      ],
    );
  }
}
