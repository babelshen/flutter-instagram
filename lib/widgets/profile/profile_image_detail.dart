import 'package:flutter/material.dart';
import 'package:instagram/models/load_image_modal.dart';

class ImageDetailScreen extends StatelessWidget {
  final ImageModel imageData;

  const ImageDetailScreen({super.key, required this.imageData});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Image Detail'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.network(
              imageData.imageUrl,
              fit: BoxFit.cover,
            ),
            const SizedBox(height: 20),
            Text(
              'Location: ${imageData.location}',
              style: const TextStyle(fontSize: 18),
            ),
            const SizedBox(height: 10),
            Text(
              'Comment: ${imageData.comment}',
              style: const TextStyle(fontSize: 18),
            ),
          ],
        ),
      ),
    );
  }
}
