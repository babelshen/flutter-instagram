import 'package:flutter/material.dart';

class ProfileAvatar extends StatelessWidget {
  final String avatar;
  const ProfileAvatar({super.key, required this.avatar});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 80,
      height: 80,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        image: DecorationImage(
          image: NetworkImage(avatar),
          fit: BoxFit.contain,
        ),
      ),
    );
  }
}
