import 'package:flutter/material.dart';
import 'package:instagram/widgets/post/custom_icon_button.dart';

class ProfileAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String nickname;
  const ProfileAppBar({super.key, required this.nickname});

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return AppBar(
      automaticallyImplyLeading: false,
      title: Text(
        nickname,
        style: theme.textTheme.titleLarge,
      ),
      actions: const <Widget>[
        Row(
          children: [
            CustomIconButton(icon: Icons.add_box_outlined, text: 'Create'),
            CustomIconButton(icon: Icons.table_rows_rounded, text: 'Menu'),
          ],
        ),
      ],
    );
  }
}
