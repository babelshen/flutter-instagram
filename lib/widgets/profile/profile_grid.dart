import 'package:flutter/material.dart';
import 'package:instagram/models/load_image_modal.dart';
import 'package:instagram/state/posts_counter_state.dart';
import 'package:instagram/widgets/profile/profile_image_detail.dart';
import 'package:provider/provider.dart';

class ProfileGrid extends StatefulWidget {
  final List<ImageModel> uploadedImages;
  final void Function(int) onDeletePost;

  const ProfileGrid(
      {super.key, required this.uploadedImages, required this.onDeletePost});

  @override
  State<ProfileGrid> createState() => _ProfileGridState();
}

class _ProfileGridState extends State<ProfileGrid> {
  int? _selectedImageIndex;

  @override
  Widget build(BuildContext context) {
    return Consumer<PostsCountManage>(builder: (context, manage, child) {
      return GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          crossAxisSpacing: 4.0,
          mainAxisSpacing: 4.0,
        ),
        itemCount: widget.uploadedImages.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              setState(() {
                _selectedImageIndex = index;
              });
            },
            child: Stack(
              children: [
                Image.network(
                  widget.uploadedImages[index].imageUrl,
                  fit: BoxFit.cover,
                ),
                if (_selectedImageIndex == index) ...[
                  Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        IconButton(
                          icon: const Icon(Icons.info),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ImageDetailScreen(
                                    imageData: widget.uploadedImages[index]),
                              ),
                            );
                          },
                        ),
                        IconButton(
                          icon: const Icon(Icons.delete),
                          onPressed: () {
                            widget
                                .onDeletePost(widget.uploadedImages[index].id);
                            manage.removePostsCounter();
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ],
            ),
          );
        },
      );
    });
  }
}
