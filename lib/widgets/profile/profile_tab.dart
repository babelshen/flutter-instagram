import 'package:flutter/material.dart';

class CustomTabWidget extends StatelessWidget {
  final IconData icon;
  final String tabText;

  const CustomTabWidget({super.key, required this.icon, required this.tabText});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Center(
      child: Column(
        children: [
          const SizedBox(
            height: 50,
          ),
          Icon(
            icon,
            color: Colors.grey,
            size: 60,
          ),
          Text(
            tabText,
            style: theme.textTheme.labelMedium,
          ),
        ],
      ),
    );
  }
}
