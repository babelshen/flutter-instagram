import 'package:flutter/material.dart';

class ProfileStatistic extends StatelessWidget {
  final int counter;
  final String title;

  const ProfileStatistic(
      {super.key, required this.counter, required this.title});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Column(
      children: [
        Text(
          '$counter',
          style: theme.textTheme.titleLarge,
        ),
        Text(
          title,
          style: theme.textTheme.titleSmall,
        ),
      ],
    );
  }
}
