import 'package:flutter/material.dart';
import 'package:instagram/state/posts_counter_state.dart';
import 'package:instagram/widgets/profile/profile_avatar.dart';
import 'package:instagram/widgets/profile/profile_statistic.dart';
import 'package:provider/provider.dart';

class ProfileHeaderWidget extends StatelessWidget {
  final String avatar;
  final int followers;
  final int following;

  const ProfileHeaderWidget({
    super.key,
    required this.avatar,
    required this.followers,
    required this.following,
  });

  @override
  Widget build(BuildContext context) {
    int postsCount = Provider.of<PostsCountManage>(context).postsCount;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ProfileAvatar(avatar: avatar),
          Row(
            children: [
              ProfileStatistic(counter: postsCount, title: 'Posts'),
              const SizedBox(width: 30),
              ProfileStatistic(counter: followers, title: 'Followers'),
              const SizedBox(width: 30),
              ProfileStatistic(counter: following, title: 'Following'),
            ],
          ),
        ],
      ),
    );
  }
}
