import 'package:flutter/material.dart';
import 'package:instagram/models/post_model.dart';

class ImageCart extends StatelessWidget {
  const ImageCart({super.key, required this.imageData});

  final PostData imageData;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: ClipRRect(
        child: Image.network(imageData.messageImage, fit: BoxFit.cover),
      ),
    );
  }
}
