import 'package:flutter/material.dart';
import 'package:instagram/models/load_image_modal.dart';
import 'package:instagram/screens/home_screen.dart';
import 'package:instagram/screens/profile_screen.dart';
import 'package:instagram/screens/saved_screen.dart';
import 'package:instagram/screens/search_screen.dart';
import 'package:instagram/screens/shop_screen.dart';

class NavigationWidget extends StatefulWidget {
  final List<ImageModel> uploadedImages;
  final Function(String, String, String) onSavePost;
  final Function(int) onDeletePost;
  const NavigationWidget({
    super.key,
    required this.uploadedImages,
    required this.onSavePost,
    required this.onDeletePost,
  });

  @override
  State<NavigationWidget> createState() => _NavigationWidgetState();
}

class _NavigationWidgetState extends State<NavigationWidget>
    with SingleTickerProviderStateMixin {
  late TabController tabController;
  int _currentIndex = 0;
  late List<Widget> tabList;

  @override
  void initState() {
    super.initState();
    tabList = [
      HomeScreen(onSavePost: widget.onSavePost),
      const SearchScreen(),
      const Shopscreen(),
      const SavedScreen(),
      ProfileScreen(
        uploadedImages: widget.uploadedImages,
        onDeletePost: widget.onDeletePost,
      ),
    ];
    tabController = TabController(vsync: this, length: tabList.length);
    tabController.addListener(() {
      setState(() {
        _currentIndex = tabController.index;
      });
    });
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  void navigationTapped(int index) {
    setState(() {
      _currentIndex = index;
      tabController.animateTo(
        index,
        duration: const Duration(milliseconds: 200),
        curve: Curves.ease,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        elevation: 0,
        iconSize: 30,
        selectedLabelStyle: const TextStyle(height: 0.0),
        currentIndex: _currentIndex,
        onTap: (index) => navigationTapped(index),
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.smart_display_outlined),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: '',
          ),
        ],
      ),
      body: TabBarView(
        controller: tabController,
        children: tabList,
      ),
    );
  }
}
