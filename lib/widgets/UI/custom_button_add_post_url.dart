import 'package:flutter/material.dart';
import 'package:instagram/data/users_data.dart';
import 'package:instagram/widgets/modals/add_post_modal.dart';

class CustomFloatingActionButton extends StatelessWidget {
  final void Function(UsersData) onAddPost;
  final void Function(String, String, String) onSavePost;

  const CustomFloatingActionButton({
    super.key,
    required this.onAddPost,
    required this.onSavePost,
  });

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        showModalBottomSheet(
          context: context,
          isScrollControlled: true,
          builder: (context) => SingleChildScrollView(
            padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).viewInsets.bottom,
            ),
            child: AddPostModal(
              onAddPost: onAddPost,
              onSavePost: onSavePost,
            ),
          ),
        );
      },
      child: const Icon(Icons.add),
    );
  }
}
