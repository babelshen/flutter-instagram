import 'package:flutter/material.dart';
import 'package:instagram/state/theme.model.dart';
import 'package:provider/provider.dart';

class EditProfileButton extends StatelessWidget {
  final VoidCallback editProfileCallback;

  const EditProfileButton({
    super.key,
    required this.editProfileCallback,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Expanded(
      child: GestureDetector(
        onTap: editProfileCallback,
        child: Container(
          height: 30,
          decoration: BoxDecoration(
            color: Provider.of<ChangeTheme>(context).isDarkThemeSelected() ? Colors.grey.shade800 : Colors.grey.shade200,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.2),
                spreadRadius: 1,
                blurRadius: 3,
                offset: const Offset(0, 2),
              ),
            ],
          ),
          child: Center(
            child: Text(
              'Edit Profile',
              style: theme.textTheme.titleMedium,
            ),
          ),
        ),
      ),
    );
  }
}
