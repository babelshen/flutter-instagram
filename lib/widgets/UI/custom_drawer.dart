import 'package:flutter/material.dart';
import 'package:instagram/data/myprofile_data.dart';
import 'package:instagram/state/theme.model.dart';
import 'package:instagram/widgets/profile/profile_avatar.dart';
import 'package:provider/provider.dart';

class CustomDrawer extends StatelessWidget {
  final bool isFloatingActionButtonEnabled;
  final FloatingActionButtonLocation? fabLocation;
  final Function(bool) onFloatingActionButtonEnabledChanged;
  final Function(FloatingActionButtonLocation?) onFabLocationChanged;

  const CustomDrawer({
    super.key,
    required this.isFloatingActionButtonEnabled,
    required this.fabLocation,
    required this.onFloatingActionButtonEnabledChanged,
    required this.onFabLocationChanged,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Consumer<ChangeTheme>(
      builder: (context, themeState, _) {
        return Drawer(
          child: ListView(
            children: <Widget>[
              DrawerHeader(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    ProfileAvatar(avatar: myProfileData.avatar),
                    Text(
                      myProfileData.name,
                      style: theme.textTheme.labelLarge,
                    ),
                  ],
                ),
              ),
              SwitchListTile(
                title: const Text(
                  'Dark Theme',
                ),
                value: themeState.isDarkThemeSelected(),
                onChanged: (value) {
                  themeState.setDarkThemeSelected(value);
                },
                activeColor: Colors.white,
                activeTrackColor: Colors.blue,
                inactiveThumbColor: Colors.white,
                inactiveTrackColor: Colors.grey.withOpacity(.5),
                trackOutlineColor:
                    MaterialStateProperty.all(Colors.transparent),
              ),
              SwitchListTile(
                title: const Text(
                  'Activate the "Add Post" button on the home page',
                ),
                value: isFloatingActionButtonEnabled,
                activeColor: Colors.white,
                activeTrackColor: Colors.blue,
                inactiveThumbColor: Colors.white,
                inactiveTrackColor: Colors.grey.withOpacity(.5),
                trackOutlineColor:
                    MaterialStateProperty.all(Colors.transparent),
                onChanged: onFloatingActionButtonEnabledChanged,
              ),
              if (isFloatingActionButtonEnabled)
                ListTile(
                  title: const Text('Placement of the "Add Post" button'),
                  trailing: DropdownButton<FloatingActionButtonLocation>(
                    value: fabLocation,
                    onChanged: onFabLocationChanged,
                    hint: const Text('Placement'),
                    items: const [
                      DropdownMenuItem(
                        value: FloatingActionButtonLocation.startFloat,
                        child: Text('Left'),
                      ),
                      DropdownMenuItem(
                        value: FloatingActionButtonLocation.centerFloat,
                        child: Text('Center'),
                      ),
                      DropdownMenuItem(
                        value: FloatingActionButtonLocation.endFloat,
                        child: Text('Right'),
                      ),
                    ],
                  ),
                ),
            ],
          ),
        );
      },
    );
  }
}
