import 'package:flutter/material.dart';

class EditCommentField extends StatefulWidget {
  const EditCommentField({super.key});

  @override
  State<EditCommentField> createState() => _EditCommentFieldState();
}

class _EditCommentFieldState extends State<EditCommentField> {
  late String newComment;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Edit Comment'),
      content: TextFormField(
        onChanged: (value) {
          setState(() {
            newComment = value;
          });
        },
        decoration: const InputDecoration(
          hintText: 'Enter your comment...',
        ),
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: const Text('Cancel'),
        ),
        TextButton(
          onPressed: () {
            Navigator.of(context).pop(newComment);
          },
          child: const Text('Save'),
        ),
      ],
    );
  }
}
