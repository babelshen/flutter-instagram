import 'package:flutter/material.dart';
import 'package:instagram/models/post_model.dart';
import 'package:instagram/widgets/post/button_without_state.dart';
import 'package:instagram/widgets/post/like.button.dart';
import 'package:instagram/widgets/modals/modal_window_info_link.dart';
import 'package:instagram/widgets/post/comments_block.dart';
import 'package:instagram/widgets/post/likes_block.dart';
import 'package:instagram/widgets/post/save_post_button.dart';
import 'package:instagram/widgets/stories/circle_story.dart';

class ImageDetailScreen extends StatefulWidget {
  const ImageDetailScreen({super.key, required this.imageData});
  final PostData imageData;

  @override
  State<ImageDetailScreen> createState() => _ImageDetailScreenState();
}

class _ImageDetailScreenState extends State<ImageDetailScreen> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: const Text(
          'Interesting',
          textAlign: TextAlign.center,
        ),
        centerTitle: true,
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(1),
          child: Container(
            height: 1,
          ),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 10),
            Material(
              child: ListTile(
                leading: CircleStory(avatar: widget.imageData.avatar),
                title: Text(
                  widget.imageData.name,
                  style: theme.textTheme.titleMedium,
                ),
                trailing: GestureDetector(
                  onTap: () {
                    showModalBottomSheet(
                      context: context,
                      builder: (BuildContext context) {
                        return PostModalWindow();
                      },
                    );
                  },
                  child: const Icon(Icons.more_horiz),
                ),
              ),
            ),
            const SizedBox(height: 10),
            Hero(
              tag: widget.imageData.id,
              flightShuttleBuilder: (flightContext, animation, flightDirection,
                  fromHeroContext, toHeroContext) {
                Widget fromHero = fromHeroContext.widget;

                return RotationTransition(turns: animation, child: fromHero);
              },
              transitionOnUserGestures: true,
              placeholderBuilder: (context, heroSize, child) => Opacity(
                opacity: .1,
                child: SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: Image.network(
                    widget.imageData.messageImage,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Image.network(
                  widget.imageData.messageImage,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Row(
                  children: [
                    LikeButton(),
                    ButtonWithoutState(
                        image: Icons.comment_bank_outlined, text: 'Comment'),
                    ButtonWithoutState(
                        image: Icons.message_rounded, text: 'Send'),
                  ],
                ),
                SavePostButton(
                  usersList: widget.imageData,
                )
              ],
            ),
            const SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.only(left: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  LikesBlock(listLikes: widget.imageData.listLikes),
                  const SizedBox(height: 10),
                  CommentsBlock(
                    name: widget.imageData.name,
                    messageText: widget.imageData.messageText,
                    comments: widget.imageData.comments,
                  ),
                  const SizedBox(height: 10),
                  Text(
                    'Today',
                    style: theme.textTheme.labelMedium,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
