import 'package:flutter/material.dart';
import 'package:instagram/models/product_model.dart';
import 'package:instagram/state/order_model.dart';
import 'package:provider/provider.dart';

class OrderCard extends StatelessWidget {
  final Product product;

  const OrderCard({super.key, required this.product});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(product.title),
      leading: Image.network(
        product.image,
        height: 100,
        width: 100,
        fit: BoxFit.contain,
      ),
      trailing: IconButton(
        icon: const Icon(Icons.delete),
        onPressed: () {
          Provider.of<OrdersState>(context, listen: false)
              .removeOrderById(product.id);
          Navigator.pop(context);
        },
      ),
    );
  }
}
