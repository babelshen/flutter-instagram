import 'package:flutter/material.dart';

class CircleStory extends StatelessWidget {
  final String avatar;
  const CircleStory({super.key, required this.avatar});

  @override
  Widget build(BuildContext context) {
    return ClipOval(
      child: Image(
        image: NetworkImage(avatar),
        height: 60,
        width: 60,
        fit: BoxFit.cover,
      ),
    );
  }
}
