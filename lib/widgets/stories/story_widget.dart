import 'package:flutter/material.dart';
import 'package:instagram/data/users_data.dart';
import 'package:instagram/widgets/stories/circle_story.dart';

class StoryWidget extends StatelessWidget {
  final UsersData user;
  const StoryWidget({super.key, required this.user});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Stack(
        children: [
          Container(
            height: 80,
            width: 80,
            decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.circular(50),
                border: Border.all(color: Colors.pink, width: 3)),
            child: CircleStory(avatar: user.avatar),
          ),
          Positioned(
            bottom: 10,
            left: 0,
            right: 0,
            child: Container(
              width: 80,
              color: Colors.transparent,
              child: Text(
                user.name,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
