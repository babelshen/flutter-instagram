import 'package:flutter/material.dart';
import 'package:instagram/data/myprofile_data.dart';
import 'package:instagram/data/users_data.dart';
import 'package:instagram/state/posts_counter_state.dart';
import 'package:instagram/widgets/ui/custom_text_field.dart';
import 'package:provider/provider.dart';

class AddPostModal extends StatefulWidget {
  final Function(UsersData) onAddPost;
  final void Function(String, String, String) onSavePost;

  const AddPostModal({
    super.key,
    required this.onAddPost,
    required this.onSavePost,
  });

  @override
  State<AddPostModal> createState() => _AddPostModalState();
}

class _AddPostModalState extends State<AddPostModal> {
  final TextEditingController imageUrlController = TextEditingController();
  final TextEditingController locationController = TextEditingController();
  final TextEditingController commentController = TextEditingController();
  String? errorMessage;

  @override
  void dispose() {
    imageUrlController.dispose();
    locationController.dispose();
    commentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CustomTextField(
            controller: imageUrlController,
            labelText: 'Image URL',
            prefixIcon: Icons.image_outlined,
          ),
          CustomTextField(
            controller: locationController,
            labelText: 'Location',
            prefixIcon: Icons.location_city_outlined,
          ),
          CustomTextField(
            controller: commentController,
            labelText: 'Comment',
            prefixIcon: Icons.comment_outlined,
          ),
          errorMessage != null
              ? Text(
                  errorMessage!,
                  style: const TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                )
              : const SizedBox(),
          ElevatedButton(
            onPressed: () {
              if (imageUrlController.text.isEmpty ||
                  locationController.text.isEmpty ||
                  commentController.text.isEmpty) {
                setState(() {
                  errorMessage = 'All fields are required!';
                });
              } else {
                String id = 'id-${DateTime.now().millisecondsSinceEpoch}';
                widget.onAddPost(
                  UsersData(
                    id: id,
                    name: myProfileData.name,
                    avatar: myProfileData.avatar,
                    location: locationController.text,
                    messageImage: imageUrlController.text,
                    messageText: commentController.text,
                    comments: 0,
                    listLikes: {},
                    reel: false,
                  ),
                );
                widget.onSavePost(
                  imageUrlController.text,
                  locationController.text,
                  commentController.text,
                );
                Provider.of<PostsCountManage>(context, listen: false)
                    .addPostsCounter();
                Navigator.of(context).pop();
              }
            },
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(Colors.blue),
              foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
              overlayColor: MaterialStateProperty.all<Color>(
                  const Color.fromARGB(255, 0, 35, 94)),
            ),
            child: const Text('Send'),
          ),
        ],
      ),
    );
  }
}
