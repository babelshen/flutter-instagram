import 'package:flutter/material.dart';

class PostModalWindowItem {
  final IconData icon;
  final String text;

  PostModalWindowItem({required this.icon, required this.text});
}

class PostModalWindowButton extends StatelessWidget {
  final PostModalWindowItem option;

  const PostModalWindowButton({super.key, required this.option});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        print('Pressed ${option.text}');
        Navigator.pop(context);
      },
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
        child: Row(
          children: [
            Icon(option.icon),
            const SizedBox(width: 8),
            Text(option.text),
          ],
        ),
      ),
    );
  }
}
