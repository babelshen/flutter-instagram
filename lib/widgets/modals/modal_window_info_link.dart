import 'package:flutter/material.dart';
import 'package:instagram/widgets/modals/modal_window_button.dart';

class PostModalWindow extends StatelessWidget {
  final List<PostModalWindowItem> modalOptions = [
    PostModalWindowItem(icon: Icons.share, text: 'Share'),
    PostModalWindowItem(icon: Icons.link, text: 'Link'),
    PostModalWindowItem(icon: Icons.save, text: 'Save'),
    PostModalWindowItem(icon: Icons.repeat, text: 'Block'),
    PostModalWindowItem(icon: Icons.qr_code, text: 'QR-code'),
  ];

  PostModalWindow({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ListView(
          shrinkWrap: true,
          children: [
            ...modalOptions
                .map((option) => PostModalWindowButton(option: option)),
          ],
        ),
      ],
    );
  }
}
