import 'package:flutter/material.dart';
import 'package:instagram/state/order_model.dart';
import 'package:instagram/state/theme.model.dart';
import 'package:instagram/widgets/ui/order_card.dart';
import 'package:provider/provider.dart';

class OrdersModalWindow extends StatelessWidget {
  const OrdersModalWindow({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        showModalBottomSheet<void>(
          context: context,
          builder: (BuildContext context) {
            return Consumer<OrdersState>(
              builder: (context, ordersState, _) {
                return Container(
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ListView.builder(
                        shrinkWrap: true,
                        itemCount: ordersState.orderedProducts.length,
                        itemBuilder: (context, index) {
                          return Row(
                            children: [
                              Expanded(
                                child: OrderCard(
                                    product:
                                        ordersState.orderedProducts[index]),
                              ),
                            ],
                          );
                        },
                      ),
                    ],
                  ),
                );
              },
            );
          },
        );
      },
      child: Container(
        height: 30,
        width: 30,
        decoration: BoxDecoration(
          color: Provider.of<ChangeTheme>(context).isDarkThemeSelected() ? Colors.grey.shade800 : Colors.grey.shade200,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.2),
              spreadRadius: 1,
              blurRadius: 3,
              offset: const Offset(0, 2),
            ),
          ],
        ),
        child: const Center(
          child: Icon(
            Icons.shopping_cart,
            size: 15,
          ),
        ),
      ),
    );
  }
}
