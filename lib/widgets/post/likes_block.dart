import 'package:flutter/material.dart';

class LikesBlock extends StatelessWidget {
  final Set<String> listLikes;

  const LikesBlock({super.key, required this.listLikes});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Row(
      children: [
        Text(
          '${listLikes.length} likes: ',
          style: theme.textTheme.titleSmall,
        ),
        Text(
          listLikes.isNotEmpty ? listLikes.first : '',
          style: theme.textTheme.titleMedium,
        ),
        Text(
          listLikes.isNotEmpty ? ' and another' : '',
          style: theme.textTheme.titleSmall,
        ),
      ],
    );
  }
}
