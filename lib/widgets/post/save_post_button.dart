import 'package:flutter/material.dart';
import 'package:instagram/models/post_model.dart';
import 'package:instagram/state/save_posts_state.dart';
import 'package:provider/provider.dart';

class SavePostButton extends StatelessWidget {
  final PostData usersList;
  const SavePostButton({super.key, required this.usersList});

  @override
  Widget build(BuildContext context) {
    return Consumer<SavePostManage>(builder: (context, savePostManager, _) {
      bool isSavedPost = savePostManager.checkSavePost(usersList.id);
      return InkWell(
        onTap: () {
          if (!isSavedPost) {
            savePostManager.addPost(usersList);
          } else {
            savePostManager.removePost(usersList.id);
          }
        },
        child: AnimatedContainer(
          duration: const Duration(milliseconds: 300),
          decoration: BoxDecoration(
            color:
                isSavedPost ? Colors.pink.withOpacity(.5): Colors.transparent,
            borderRadius: BorderRadius.circular(100),
          ),
          padding: const EdgeInsets.all(8),
          child: Image.asset(
            'assets/save.png',
            height: 28,
          ),
        ),
      );
    });
  }
}
