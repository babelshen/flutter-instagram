import 'package:flutter/material.dart';

class ButtonWithoutState extends StatelessWidget {
  final IconData image;
  final String text;
  const ButtonWithoutState(
      {super.key, required this.image, required this.text});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(
        image,
        size: 25,
      ),
      onPressed: () {
        print('$text button pressed');
      },
    );
  }
}
