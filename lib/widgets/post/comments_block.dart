import 'package:flutter/material.dart';

class CommentsBlock extends StatelessWidget {
  final String name;
  final String messageText;
  final int comments;

  const CommentsBlock(
      {super.key,
      required this.name,
      required this.messageText,
      required this.comments});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              name,
              style: theme.textTheme.titleMedium,
            ),
            Text(
              messageText.length > 30
                  ? ': ${messageText.substring(0, 30)}...'
                  : ': $messageText',
              style: theme.textTheme.titleSmall,
            ),
          ],
        ),
        Text(
          'View all comments ($comments)',
          style: theme.textTheme.labelMedium,
        ),
      ],
    );
  }
}
