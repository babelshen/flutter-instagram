import 'package:flutter/material.dart';
import 'package:instagram/data/users_data.dart';
import 'package:instagram/widgets/post/button_without_state.dart';
import 'package:instagram/widgets/post/like.button.dart';
import 'package:instagram/widgets/modals/modal_window_info_link.dart';
import 'package:instagram/widgets/post/save_post_button.dart';
import 'package:instagram/widgets/stories/circle_story.dart';
import 'package:instagram/widgets/post/comments_block.dart';
import 'package:instagram/widgets/post/likes_block.dart';
import 'package:instagram/widgets/post/post_image.dart';

class PostCart extends StatelessWidget {
  final int index;

  const PostCart({super.key, required this.index});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Column(
      children: [
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 70,
          child: Center(
            child: ListTile(
              leading: CircleStory(avatar: usersList[index].avatar),
              title: Text(
                usersList[index].name,
                style: theme.textTheme.titleMedium,
              ),
              subtitle: Text(
                usersList[index].location,
                style: theme.textTheme.labelSmall,
              ),
              trailing: GestureDetector(
                onTap: () {
                  showModalBottomSheet(
                    context: context,
                    builder: (BuildContext context) {
                      return PostModalWindow();
                    },
                  );
                },
                child: const Icon(Icons.more_horiz),
              ),
            ),
          ),
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width,
          child: PostImage(image: usersList[index].messageImage),
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Row(
                      children: [
                        LikeButton(),
                        ButtonWithoutState(
                            image: Icons.comment_bank_outlined, text: 'Comment'),
                        ButtonWithoutState(
                            image: Icons.message_rounded, text: 'Send'),
                      ],
                    ),
                    SavePostButton(usersList: usersList[index]),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 5, bottom: 5),
                      child: LikesBlock(listLikes: usersList[index].listLikes),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 5),
                      child: CommentsBlock(
                        name: usersList[index].name,
                        messageText: usersList[index].messageText,
                        comments: usersList[index].comments,
                      ),
                    ),
                    Text(
                      'Today',
                      style: theme.textTheme.labelMedium,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
