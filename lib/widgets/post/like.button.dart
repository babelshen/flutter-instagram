import 'package:flutter/material.dart';

class LikeButton extends StatefulWidget {
  const LikeButton({super.key});

  @override
  State<LikeButton> createState() => _LikeButtonState();
}

class _LikeButtonState extends State<LikeButton> {
  bool _isLiked = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: _isLiked
            ? [
                BoxShadow(
                  color: Colors.pink.withOpacity(0.1),
                  blurRadius: 10,
                  offset: const Offset(0, 0),
                ),
              ]
            : null,
      ),
      child: IconButton(
        onPressed: () {
          setState(() {
            _isLiked = !_isLiked;
          });
          print('Like button pressed');
        },
        icon: Icon(
          _isLiked ? Icons.favorite : Icons.favorite_outline,
          size: 25,
          color: _isLiked ? Colors.red : Theme.of(context).primaryColor,
        ),
      ),
    );
  }
}
