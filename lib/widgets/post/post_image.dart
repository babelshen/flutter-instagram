import 'package:flutter/material.dart';

class PostImage extends StatelessWidget {
  final String image;

  const PostImage({super.key, required this.image});

  @override
  Widget build(BuildContext context) {
    return Image(
      image: NetworkImage(image),
      fit: BoxFit.cover,
      height: 400,
    );
  }
}
